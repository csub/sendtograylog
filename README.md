# sendToGraylog

Sends a flat file to graylog line by line

## Prerequisites

Requires the following installed locally

```
bash
netcat
curl
```

## Usage

```
sendToGraylog -i <filename> [-h] [-s <syslog server>] [-i]
  -h           optional  Print this help message
  -i <file>    required  input file
  -s <server>  optional  syslog server. default is graylog.ops.csub.edu.
  -i install   optional  install. copies to /usr/local/bin.
```

## ToDo

  - Add option to automatically download what was just uploaded as csv
  - Add proper install/update options
